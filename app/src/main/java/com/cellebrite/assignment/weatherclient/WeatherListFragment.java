package com.cellebrite.assignment.weatherclient;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cellebrite.assignment.weatherclient.database.WeatherContract;
import com.cellebrite.assignment.weatherclient.database.WeatherProvider;

/**
 * Fragment that displays a pool of weather data from database in descending order
 * Created by Evgeniy Mishustin on 06/05/2016.
 */
public class WeatherListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {


    private WeatherListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_list, container, false);
        ListView list = (ListView) view.findViewById(R.id.weather_list);
        adapter = new WeatherListAdapter(getActivity(), null , 0);
        list.setAdapter(adapter);
        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), WeatherProvider.CONTENT_URI, WeatherContract.PROJECTION, null, null, WeatherContract.WeatherTable.ENTRY_TIME +" DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //if we start application for the first time - query weather immediately and schedule repeats
        if(data.getCount()==0){
            getActivity().startService(new Intent(getActivity(), WeatherPullService.class));
            Utils.scheduleRepeatingAlarm(getActivity());
        }else {
            adapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }


}
