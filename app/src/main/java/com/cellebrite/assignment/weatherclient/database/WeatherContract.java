package com.cellebrite.assignment.weatherclient.database;

import android.provider.BaseColumns;

/**
 * Represents a table/s in the database
 * Created by Evgeniy Mishustin on 06/05/2016.
 */
public final class WeatherContract {

    public static abstract class WeatherTable implements BaseColumns {
        public static final String TABLE_NAME = "weather";
        public static final String ENTRY_ID = "weather_id";
        public static final String ENTRY_TIME = "time";
        public static final String TEMP = "temp";
        public static final String DESCRIPTION = "description";
        public static final String ICON = "icon";

    }

    public static String[] PROJECTION = {
            WeatherTable._ID,
            WeatherTable.ENTRY_ID,
            WeatherTable.ENTRY_TIME,
            WeatherTable.TEMP,
            WeatherTable.DESCRIPTION,
            WeatherTable.ICON

    };
}
