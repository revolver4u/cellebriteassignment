package com.cellebrite.assignment.weatherclient.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.cellebrite.assignment.weatherclient.Utils;

/**
 * Rescheduling alarms on boot complete
 * Created by Evgeniy Mishustin on 05/05/2016.
 */
public class BootReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            Utils.scheduleRepeatingAlarm(context);
        }
    }
}
