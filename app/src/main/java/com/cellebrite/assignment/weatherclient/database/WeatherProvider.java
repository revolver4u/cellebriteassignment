package com.cellebrite.assignment.weatherclient.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * A simple contact provider
 * implemented mainly to reload data on-the-fly
 * Created by Evgeniy Mishustin on 06/05/2016.
 */
public class WeatherProvider extends ContentProvider {

    private WeatherDbHelper dbHelper;

    private static final String AUTHORITY = "com.cellebrite.assignment.weatherclient.database";
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    // used by the UriMacher
    private static final int TABLE_ID = 1;
    private static final int ENTRY_ID = 2;
    static {
        uriMatcher.addURI(AUTHORITY, WeatherContract.WeatherTable.TABLE_NAME, TABLE_ID);
        uriMatcher.addURI(AUTHORITY, WeatherContract.WeatherTable.TABLE_NAME + "/#", ENTRY_ID);
    }

    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY +"/" + WeatherContract.WeatherTable.TABLE_NAME);

    @Override
    public boolean onCreate() {
        dbHelper = new WeatherDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(WeatherContract.WeatherTable.TABLE_NAME);
        switch (uriMatcher.match(uri)){
            case TABLE_ID:
                break;
            case ENTRY_ID:
                queryBuilder.appendWhere(WeatherContract.WeatherTable._ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(database, projection, selection, args, null, null, sortOrder);
        //notify all the possible listeners
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        int uriType = uriMatcher.match(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        long id = 0;
        switch (uriType){
            case TABLE_ID:
                id = database.insert(WeatherContract.WeatherTable.TABLE_NAME, null, contentValues);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(WeatherContract.WeatherTable.TABLE_NAME +"/" + id);
    }

    //wont be used in this app
    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    //wont be used in this app
    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
