package com.cellebrite.assignment.weatherclient;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.cellebrite.assignment.weatherclient.database.WeatherContract;
import com.cellebrite.assignment.weatherclient.database.WeatherProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * This class will query the WeatherApi Server to get current weather data
 * in JSON format, parse it, and write it as row in database
 * Created by Evgeniy Mishustin on 05/05/2016.
 */
public class WeatherPullService extends IntentService {

    //PETAH TIKVA CITY ID = 6693674
    private final static String API_CALL = "http://api.openweathermap.org/data/2.5/weather?id=6693674&units=metric&APPID=5aa7cc1d5cacc0c3d2746ef264c047a3";
    private final static long NETWORK_TIMEOUT = 30;

    public WeatherPullService() {
        super(WeatherPullService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (Utils.checkNetworkState(this)) {
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(API_CALL, null, future, future);
            Volley.newRequestQueue(this).add(jsObjRequest);
            try {
                JSONObject response = future.get(NETWORK_TIMEOUT, TimeUnit.SECONDS); // this will block
                if (response != null) {
                    getContentResolver().insert(WeatherProvider.CONTENT_URI, parseResponse(response));
                }

            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
        }


    }

    //parses the JSON response from open weather API to content values
    private ContentValues parseResponse(JSONObject response) {
        try {
            ContentValues values = new ContentValues();
            values.put(WeatherContract.WeatherTable.ENTRY_ID, response.getString("id"));
            values.put(WeatherContract.WeatherTable.ENTRY_TIME, System.currentTimeMillis());
            values.put(WeatherContract.WeatherTable.TEMP, response.getJSONObject("main").getString("temp"));
            values.put(WeatherContract.WeatherTable.DESCRIPTION, response.getJSONArray("weather").getJSONObject(0).getString("description"));
            values.put(WeatherContract.WeatherTable.ICON, response.getJSONArray("weather").getJSONObject(0).getString("icon"));
            return values;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
