package com.cellebrite.assignment.weatherclient.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * SQLite Open helper
 * Created by Evgeniy Mishustin on 05/05/2016.
 */
public class WeatherDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "weather.db";

    private static final String CREATE_STATEMENT = "CREATE TABLE " +
            WeatherContract.WeatherTable.TABLE_NAME + " (" +
            WeatherContract.WeatherTable._ID + " INTEGER PRIMARY KEY," +
            WeatherContract.WeatherTable.ENTRY_ID + " TEXT," +
            WeatherContract.WeatherTable.ENTRY_TIME + " INTEGER, " +
            WeatherContract.WeatherTable.DESCRIPTION + " TEXT," +
            WeatherContract.WeatherTable.TEMP + " TEXT," +
            WeatherContract.WeatherTable.ICON + " TEXT" +
            ")";


    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + WeatherContract.WeatherTable.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
