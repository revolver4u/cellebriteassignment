package com.cellebrite.assignment.weatherclient;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cellebrite.assignment.weatherclient.database.WeatherContract;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Adapts a database row to a row in ListView
 * Created by Evgeniy Mishustin on 06/05/2016.
 */
public class WeatherListAdapter extends CursorAdapter {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

    public WeatherListAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_weather, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView temp = (TextView)view.findViewById(R.id.temperature);
        temp.setText(normalizeTemperature(cursor.getString(cursor.getColumnIndexOrThrow(WeatherContract.WeatherTable.TEMP))));
        TextView time = (TextView) view.findViewById(R.id.time);
        time.setText(normalizeTime(cursor.getLong(cursor.getColumnIndexOrThrow(WeatherContract.WeatherTable.ENTRY_TIME))));
        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(cursor.getString(cursor.getColumnIndexOrThrow(WeatherContract.WeatherTable.DESCRIPTION)));
        ImageView icon = (ImageView)view.findViewById(R.id.weatherIcon);
        Glide.with(context).load(getUrl(cursor.getString(cursor.getColumnIndexOrThrow(WeatherContract.WeatherTable.ICON)))).into(icon);
    }

    //rounds the temperature and adds celsius degree symbol
    private String normalizeTemperature(String dbTemperature){
        float f = Float.valueOf(dbTemperature);
        return (Math.round(f) + "\u2103");
    }

    //normalizes time to human readable format
    private String normalizeTime(long time){
        Date date = new Date(time);
        return simpleDateFormat.format(date);
    }

    private String getUrl(String icon){
        return ("http://openweathermap.org/img/w/" + icon +".png");
    }
}
