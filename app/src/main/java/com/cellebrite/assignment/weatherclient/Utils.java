package com.cellebrite.assignment.weatherclient;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;


/**
 * Some reused methods
 * Created by Evgeniy Mishustin on 05/05/2016.
 */
public class Utils {

    //checks the network state returns false on no network to save useless requests to server
    public static boolean checkNetworkState(Context context){
        ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    //schedules an alarm that will be triggered every 10 minutes
    public static void scheduleRepeatingAlarm(Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, WeatherPullService.class);
        PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);
        manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 1000*60*10,
                1000*60*10, alarmIntent);
    }

}
